#!/bin/bash
[[ ! -e VERSION.tex ]] && exit 1
VERSION="$(<VERSION.tex \
  sed -rn 's#(....)/(..)/(..) v([^ ]*) fithesis3 MU thesis class#v\4#p')"
[[ -z "$VERSION" ]] && exit 2

git tag "$VERSION"
git push all master --tags --force
