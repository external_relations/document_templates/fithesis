// Remember user's faculty.
if ("localStorage" in window) {
  document.documentElement.className = localStorage.getItem("faculty") || "";
  var timeout;
  [].forEach.call(document.querySelectorAll("a.faculty"), function(a) {
    a.onclick = function() {
      clearTimeout(timeout);
      switchFaculty();
    }; a.onmouseover = function() {
      clearTimeout(timeout);
      timeout = setTimeout(switchFaculty, 500);
    }; a.onmouseout = function() {
      clearTimeout(timeout);
    };

    function switchFaculty() {
      var faculty = a.className.replace(/faculty/g, "").replace(/\s/g, "");
      document.documentElement.className = faculty;
      localStorage.setItem("faculty", faculty);
    }
  });
}
