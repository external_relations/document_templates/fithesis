#!/bin/bash
# Uploads an updated version of the Overleaf template for faculty $1 to
# Overleaf.
TMPDIR=`mktemp -d`
trap 'rm -rf $TMPDIR' EXIT

set -e
. overleaf-meta/$1/overleaf-upload.def
while ! git clone https://git.overleaf.com/$DOCUMENT_ID $TMPDIR
do
  sleep 5s
done
rm -rf overleaf/$1/.git
mv $TMPDIR/.git overleaf/$1/.git
cd overleaf/$1
git add .
git commit -m "Uploaded new version." || true
while ! git push
do
  sleep 5s
done
