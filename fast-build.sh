#!/bin/bash
set -e -o xtrace

make implode
make -j 4 -C logo/mu
make -j 4
make -j 4 -C example/mu
make -j 4 dist
