#!/bin/bash
# Installs fithesis4 to the faculty texlive-2015 installation

# Clean up
rm -rf /tmp/fithesis &&
# Prepare the files
mkdir /tmp/fithesis &&
unzip -d /tmp/fithesis fithesis.tds.zip &&
# Transfer the files
cd /tmp && tar czv fithesis | ssh `aisa` 'sg tex -c '\''
TEXLIVE=/packages/share/texlive-2015/texmf-local
tar xzC /tmp && cd /tmp/fithesis &&
find /tmp/fithesis -exec chmod g+w {} + &&
# Replace the currently installed class files
rm -rfv                  "$TEXLIVE"/tex/latex/fithesis &&
mv -v tex/latex/fithesis "$TEXLIVE"/tex/latex &&
# Replace the currently installed documentation
rm -rfv                  "$TEXLIVE"/doc/latex/fithesis &&
mv -v doc/latex/fithesis "$TEXLIVE"/doc/latex &&
# Replace the currently installed source files
rm -rfv                     "$TEXLIVE"/source/latex/fithesis &&
mv -v source/latex/fithesis "$TEXLIVE"/source/latex &&
# Clean up
cd .. && rm -rf fithesis && texhash
'\'   && rm -rf fithesis
